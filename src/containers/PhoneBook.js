import React from 'react';
import {StyleSheet, Image, View, Text, TouchableOpacity, Modal, ScrollView} from 'react-native';
import {connect} from "react-redux";
import {closeModal, fetchContacts, getContactByID, showModal} from "../store/action";
import FullContact from "../components/FullContact";


const styles = StyleSheet.create({
    contact: {
        flex: 2,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'black',
        marginVertical: 10,
        paddingHorizontal: 25,
        paddingVertical: 10,
        alignItems: 'stretch'

    },
    page: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        marginTop: 25,
        alignItems: 'stretch'
    },
    button: {
        borderWidth: 1,
        borderColor: 'black',
        padding: 25,
        backgroundColor: '#FFA500',
        marginTop: 60,
        marginHorizontal: 25

    },
});


class PhoneBook extends React.Component {

    componentDidMount() {
        this.props.fetchContacts();
    }

    showModal = id => {
        this.props.showModal(id);
    };

    render() {
        let contacts = null;
        if (this.props.contacts) {
            contacts = this.props.contacts.map((contact, index) => (
                <TouchableOpacity onPress={() => this.showModal(index)}
                                  style={styles.contact}
                                  key={contact.id}>
                    <Image style={{width: 50, height: 50, marginHorizontal: 10}} source={{uri: contact.photo}}/>
                    <Text style={{fontSize: 20}}>{contact.name}</Text>
                </TouchableOpacity>
            ))
        }
        return (
            <View style={styles.page}>
                <Text style={{fontWeight: 'bold', textAlign: 'center', fontSize: 20}}>My phoneBook</Text>
                <ScrollView>
                    {contacts}
                </ScrollView>
                <Modal visible={this.props.showing}
                       onRequestClose={() => {
                           console.log('Modal closed');
                       }}>
                    <FullContact/>
                    <TouchableOpacity style={styles.button} onPress={this.props.closeModal}>
                        <Text style={{fontSize: 20}}>Back to list</Text>
                    </TouchableOpacity>
                </Modal>
            </View>


        );
    }
}

const mapStateToProps = state => {
    return {
        contacts: state.contacts,
        showing: state.showing,
        contactById: state.contactById
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchContacts: () => dispatch(fetchContacts()),
        showModal: (id) => dispatch(showModal(id)),
        closeModal: () => dispatch(closeModal()),

    };

};


export default connect(mapStateToProps, mapDispatchToProps)(PhoneBook);