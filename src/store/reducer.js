import {CONTACT_REQUEST, CONTACT_FAILURE, CONTACT_SUCCESS, CONTACT_BY_ID, CLOSE_MODAL, SHOW_MODAL} from "./action";


const initialState = {
    contacts: [],
    error: null,
    showing: false,
    contactById: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CONTACT_SUCCESS:
            return {
                ...state,
                contacts: action.response,
            };
        case CONTACT_REQUEST:
            return {
                ...state,
            };
        case CONTACT_BY_ID:
            return {
                ...state,
                contactById: state.contacts[action.id],
            };
        case CLOSE_MODAL:
            return {
                ...state,
                contactById: null,
                showing: false
            };
        case SHOW_MODAL:
            return {
                ...state,
                showing: true
            };
        case CONTACT_FAILURE:
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};
export default reducer;


