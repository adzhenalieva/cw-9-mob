import axios from '../../axios-contacts';

export const CONTACT_REQUEST = 'CONTACT_REQUEST';
export const CONTACT_SUCCESS = 'CONTACT_SUCCESS';
export const CONTACT_FAILURE = 'CONTACT_FAILURE';
export const CLOSE_MODAL = "CLOSE_MODAL";
export const SHOW_MODAL = "SHOW_MODAL";
export const CONTACT_BY_ID = "CONTACT_BY_ID";

export const contactRequest = () => ({type: CONTACT_REQUEST});
export const contactSuccess = response => ({type: CONTACT_SUCCESS, response});
export const contactFailure = error => ({type: CONTACT_FAILURE, error});

export const fetchContacts = () => {
    return dispatch => {
        dispatch(contactRequest());
        axios.get('/contacts.json').then(response => {
            const contacts = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });
            dispatch(contactSuccess(contacts));
        }, error => {
            dispatch(contactFailure(error));
        });
    }
};

export const getContactByID = id => ({type: CONTACT_BY_ID, id});
export const closeModal = () => ({type: CLOSE_MODAL});
export const showModal = (id) => {
    return dispatch => {
        dispatch(getContactByID(id));
        dispatch({type: SHOW_MODAL});
    }
};