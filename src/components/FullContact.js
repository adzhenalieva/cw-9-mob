import React from "react";
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {connect} from "react-redux";

const styles = StyleSheet.create({
    list: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: 'black',
        margin: 25,
        padding: 5,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: 100,
        height: 100,
        marginVertical: 10
    },
    text: {
        margin: 25,
        fontSize: 20
    }
});

class FullContact extends React.Component {

    render() {
        return (
            <View style={styles.list}>
                <Image style={styles.image}
                       source={{uri: this.props.contactById.photo}}/>
                <Text style={styles.text}>Name: {this.props.contactById.name}   </Text>
                <Text style={styles.text}>mob: {this.props.contactById.phone}   </Text>
                <Text style={styles.text}>e-mail: {this.props.contactById.email}   </Text>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        contactById: state.contactById

    };
};

// const mapDispatchToProps = dispatch => {
//     return {
//
//
//
//
//     };
// };

export default connect(mapStateToProps)(FullContact);

