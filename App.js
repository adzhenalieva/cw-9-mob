import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import reducer from './src/store/reducer';
import PhoneBook from "./src/containers/PhoneBook";

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'center',
        marginHorizontal: 5
    },
});

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    <PhoneBook/>
                </View>
            </Provider>
        );
    }
}

